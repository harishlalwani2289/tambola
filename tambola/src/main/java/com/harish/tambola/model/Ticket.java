package com.harish.tambola.model;

import java.util.ArrayList;
import java.util.List;

public class Ticket {
    String ticketName;
    ArrayList<Integer> row1;
    ArrayList<Integer> row2;
    ArrayList<Integer> row3;

    public Ticket(String ticketName) {
        this.ticketName = ticketName;
    }

    public Ticket(){

    }

    public String getTicketName() {
        return ticketName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public ArrayList<Integer> getRow1() {
        return row1;
    }

    public void setRow1(ArrayList<Integer> row1) {
        this.row1 = row1;
    }

    public ArrayList<Integer> getRow2() {
        return row2;
    }

    public void setRow2(ArrayList<Integer> row2) {
        this.row2 = row2;
    }

    public ArrayList<Integer> getRow3() {
        return row3;
    }

    public void setRow3(ArrayList<Integer> row3) {
        this.row3 = row3;
    }
}
