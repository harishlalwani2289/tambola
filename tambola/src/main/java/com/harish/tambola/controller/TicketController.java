package com.harish.tambola.controller;

import com.harish.tambola.model.Ticket;
import com.harish.tambola.model.TicketGenerator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/tickets")
public class TicketController {

    @GetMapping
    public List<Ticket> getTickets()
    {
        List<Ticket> listOfTicket = new ArrayList<>();
        for(int i= 0 ; i< 100; i++) {
            int[][] ticket = TicketGenerator.getTicket();
            Ticket ticketModel = new Ticket("Ticket" + (i+1));

            ticketModel.setRow1((ArrayList<Integer>) createIntegerList(ticket[0]));
            ticketModel.setRow2((ArrayList<Integer>) createIntegerList(ticket[1]));
            ticketModel.setRow3((ArrayList<Integer>) createIntegerList(ticket[2]));
            listOfTicket.add(ticketModel);
        }

        return listOfTicket;
    }

    private List<Integer> createIntegerList(int ticketRow[]) {
        List<Integer> list = new ArrayList<>(ticketRow.length);

        for (int i : ticketRow) {
            list.add(Integer.valueOf(i));
        }
        return list;
    }

}
